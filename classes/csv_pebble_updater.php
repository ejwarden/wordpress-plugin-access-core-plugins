<?php
class CSVUpdaterPlugin {
	var $defaults = array(
		'csv_post_title'	  => null,
		'csv_post_post'		  => null,
		'csv_post_type'		  => null,
		'csv_post_excerpt'	  => null,
		'csv_post_date'		  => null,
		'csv_post_tags'		  => null,
		'csv_post_categories' => null,
		'csv_post_author'	  => null,
		'csv_post_slug'		  => null,
		'csv_post_parent'	  => 0,
	);

	var $log = array();

	/**
	 * Plugin's interface
	 *
	 * @return void
	 */
	function form() {

		if ('POST' == $_SERVER['REQUEST_METHOD']) {
			$this->post(compact('email_users'));
		}

		// form HTML {{{
?>

<div class="wrap">
	<h2>Update Pebble Beach Progress via CSV</h2>
	<form class="add:the-list: validate" method="post" enctype="multipart/form-data">

		<!-- File input -->
		<p><label for="csv_import">Upload update file (CSV ONLY):</label><br/>
			<input name="csv_import" id="csv_import" type="file" value="" aria-required="true" /></p>
		<p class="submit"><input type="submit" class="button" name="submit" value="Import" /></p>
	</form>
</div><!-- end wrap -->

<?php
		// end form HTML }}}

	}

	function print_messages() {
		if (!empty($this->log)) {

		// messages HTML {{{
?>

<div class="wrap">
	<?php if (!empty($this->log['error'])): ?>

	<div class="error">

		<?php foreach ($this->log['error'] as $error): ?>
			<p><?php echo $error; ?></p>
		<?php endforeach; ?>

	</div>

	<?php endif; ?>

	<?php if (!empty($this->log['notice'])): ?>

	<div class="updated fade">

		<?php foreach ($this->log['notice'] as $notice): ?>
			<p><?php echo $notice; ?></p>
		<?php endforeach; ?>

	</div>

	<?php endif; ?>
</div><!-- end wrap -->

<?php
		// end messages HTML }}}

			$this->log = array();
		}
	}

	/**
	 * Handle POST submission
	 *
	 * @param array $options
	 * @return void
	 */
	function post($options) {
		ini_set("auto_detect_line_endings", true);
		
		if (empty($_FILES['csv_import']['tmp_name'])) {
			$this->log['error'][] = 'No file uploaded, aborting.';
			$this->print_messages();
			return;
		}

		if (!current_user_can('manage_options')) {
			$this->log['error'][] = 'You don\'t have the permissions to update users. Please contact the site\'s administrator.';
			$this->print_messages();
			return;
		}

		require_once dirname(dirname(__FILE__)) . '/File_CSV_DataSource/DataSource.php';
		
		$time_start		= microtime(true);
		$csv			= new File_CSV_DataSource;
		$file			= $_FILES['csv_import']['tmp_name'];
		$this->stripBOM($file);

		if (!$csv->load($file)) {
			$this->log['error'][] = 'Failed to load file, aborting.';
			$this->print_messages();
			return;
		}
		$csv->load($file);
		
		// pad shorter rows with empty values
		$csv->symmetrize();

		// WordPress sets the correct timezone for date functions somewhere
		// in the bowels of wp_insert_post(). We need strtotime() to return
		// correct time before the call to wp_insert_post().
		$tz = get_option('timezone_string');
		if ($tz && function_exists('date_default_timezone_set')) {
			date_default_timezone_set($tz);
		}

		$skipped		= 0;
		$imported		= 0;
		$comments		= 0;
		
		global $wpdb;
		$results			= $wpdb->get_results("SELECT `CUSTOMER #` FROM ".SRIXON_USER_TABLE);
		$customer_numbers	=  array();
		
		//Store Customer IDs for Comparison of Duplicates
		foreach($results as $result){
			$customer_numbers[] = $result->{'CUSTOMER #'};
		}
		
		foreach ($csv->connect() as $csv_data) {
			if( !empty($csv_data['CUSTOMERNBR']) ) {
				
				$CUSTOMER_NUMBER		= sanitize_text_field($csv_data['CUSTOMERNBR']);
				$PEBBLE_BEACH_SALES		= sanitize_text_field($csv_data['PEBBLE_QUALIFYING_REVENUE']);
				
				$wpdb->update( SRIXON_USER_TABLE, array('PEBBLE BEACH SALES' => $PEBBLE_BEACH_SALES), array('CUSTOMER #' => $CUSTOMER_NUMBER));
				
				$imported++;
			} else {
				$skipped++;
			}
		}

		if (file_exists($file)) {
			@unlink($file);
		}

		$exec_time = microtime(true) - $time_start;

		if ($skipped) {
			$this->log['notice'][] = "<b>Skipped {$skipped} users due to errors.</b>";
		}
		$this->log['notice'][] = sprintf("<b>Updated {$imported} users in %.2f seconds.</b>", $exec_time);
		$this->print_messages();
	}
	/**
	 * Try to split lines of text correctly regardless of the platform the text
	 * is coming from.
	 */
	function split_lines($text) {
		$lines = preg_split("/(\r\n|\n|\r)/", $text);
		return $lines;
	}

	/**
	 * Convert date in CSV file to 1999-12-31 23:52:00 format
	 *
	 * @param string $data
	 * @return string
	 */
	function parse_date($data) {
		$timestamp = strtotime($data);
		if (false === $timestamp) {
			return '';
		} else {
			return date('Y-m-d H:i:s', $timestamp);
		}
	}

	/**
	 * Delete BOM from UTF-8 file.
	 *
	 * @param string $fname
	 * @return void
	 */
	function stripBOM($fname) {
		$res = fopen($fname, 'rb');
		if (false !== $res) {
			$bytes = fread($res, 3);
			if ($bytes == pack('CCC', 0xef, 0xbb, 0xbf)) {
				$this->log['notice'][] = 'Getting rid of byte order mark...';
				fclose($res);

				$contents = file_get_contents($fname);
				if (false === $contents) {
					trigger_error('Failed to get file contents.', E_USER_WARNING);
				}
				$contents = substr($contents, 3);
				$success = file_put_contents($fname, $contents);
				if (false === $success) {
					trigger_error('Failed to put file contents.', E_USER_WARNING);
				}
			} else {
				fclose($res);
			}
		} else {
			$this->log['error'][] = 'Failed to open file, aborting.';
		}
	}
}

?>

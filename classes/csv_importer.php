<?php
class CSVImporterPlugin {
    var $defaults = array(
        'csv_post_title'      => null,
        'csv_post_post'       => null,
        'csv_post_type'       => null,
        'csv_post_excerpt'    => null,
        'csv_post_date'       => null,
        'csv_post_tags'       => null,
        'csv_post_categories' => null,
        'csv_post_author'     => null,
        'csv_post_slug'       => null,
        'csv_post_parent'     => 0,
    );

    var $log = array();

    /**
     * Determine value of option $name from database, $default value or $params,
     * save it to the db if needed and return it.
     *
     * @param string $name
     * @param mixed  $default
     * @param array  $params
     * @return string
     */
    function process_option($name, $default, $params) {
        if (array_key_exists($name, $params)) {
            $value = stripslashes($params[$name]);
        } elseif (array_key_exists('_'.$name, $params)) {
            // unchecked checkbox value
            $value = stripslashes($params['_'.$name]);
        } else {
            $value = null;
        }
        $stored_value = get_option($name);
        if ($value == null) {
            if ($stored_value === false) {
                if (is_callable($default) &&
                    method_exists($default[0], $default[1])) {
                    $value = call_user_func($default);
                } else {
                    $value = $default;
                }
                add_option($name, $value);
            } else {
                $value = $stored_value;
            }
        } else {
            if ($stored_value === false) {
                add_option($name, $value);
            } elseif ($stored_value != $value) {
                update_option($name, $value);
            }
        }
        return $value;
    }

    /**
     * Plugin's interface
     *
     * @return void
     */
    function form() {
        $email_users = $this->process_option('srixon_csv_importer_email_users',
            'yes', $_POST);

        if ('POST' == $_SERVER['REQUEST_METHOD']) {
            $this->post(compact('email_users'));
        }

        // form HTML {{{
?>

<div class="wrap">
    <h2>Import Srixon User CSV</h2>
    <form class="add:the-list: validate" method="post" enctype="multipart/form-data">
        <!-- Import as draft -->
        <p>
        <label><input name="email_users" type="checkbox" <?php if( $email_users == 'yes' ): ?>checked<?php endif; ?> value="yes" /> Email Users their site User Keys?</label>
        </p>

        <!-- File input -->
        <p><label for="csv_import">Upload file:</label><br/>
            <input name="csv_import" id="csv_import" type="file" value="" aria-required="true" /></p>
        <p class="submit"><input type="submit" class="button" name="submit" value="Import" /></p>
    </form>
</div><!-- end wrap -->

<?php
        // end form HTML }}}

    }

    function print_messages() {
        if (!empty($this->log)) {

        // messages HTML {{{
?>

<div class="wrap">
    <?php if (!empty($this->log['error'])): ?>

    <div class="error">

        <?php foreach ($this->log['error'] as $error): ?>
            <p><?php echo $error; ?></p>
        <?php endforeach; ?>

    </div>

    <?php endif; ?>

    <?php if (!empty($this->log['notice'])): ?>

    <div class="updated fade">

        <?php foreach ($this->log['notice'] as $notice): ?>
            <p><?php echo $notice; ?></p>
        <?php endforeach; ?>

    </div>

    <?php endif; ?>
</div><!-- end wrap -->

<?php
        // end messages HTML }}}

            $this->log = array();
        }
    }

    /**
     * Handle POST submission
     *
     * @param array $options
     * @return void
     */
    function post($options) {
		ini_set("auto_detect_line_endings", true);
		
        if (empty($_FILES['csv_import']['tmp_name'])) {
            $this->log['error'][] = 'No file uploaded, aborting.';
            $this->print_messages();
            return;
        }

        if (!current_user_can('manage_options')) {
            $this->log['error'][] = 'You don\'t have the permissions to add new users. Please contact the site\'s administrator.';
            $this->print_messages();
            return;
        }

        require_once dirname(dirname(__FILE__)) . '/File_CSV_DataSource/DataSource.php';
		require_once dirname(dirname(__FILE__)) . '/email-templates/Welcome_Email_Template.php';
		require_once 'encryptomatic.class.php';
		
        $time_start		= microtime(true);
        $csv			= new File_CSV_DataSource;
        $file			= $_FILES['csv_import']['tmp_name'];
        $this->stripBOM($file);

        if (!$csv->load($file)) {
            $this->log['error'][] = 'Failed to load file, aborting.';
            $this->print_messages();
            return;
        }
		$csv->load($file);
		
        // pad shorter rows with empty values
        $csv->symmetrize();

        // WordPress sets the correct timezone for date functions somewhere
        // in the bowels of wp_insert_post(). We need strtotime() to return
        // correct time before the call to wp_insert_post().
        $tz = get_option('timezone_string');
        if ($tz && function_exists('date_default_timezone_set')) {
            date_default_timezone_set($tz);
        }

        $skipped		= 0;
        $imported		= 0;
        $comments		= 0;
		$email_users	= $_POST['email_users'];
		
		global $wpdb;
		$results			= $wpdb->get_results("SELECT `EMAIL` FROM ".SRIXON_USER_TABLE);
		$customer_emails	=  array();
		
		//Store Customer IDs for Comparison of Duplicates
		foreach($results as $result){
			$customer_emails[] = $result->{'EMAIL'};
		}
		
		foreach ($csv->connect() as $csv_data) {
			if( !empty($csv_data['EMAIL']) && !in_array($csv_data['EMAIL'], $customer_emails) ) {
				$USER_KEY				= $this->generate_user_key();
				$encrypto				= new Encryptomatic($USER_KEY, 'encrypt');
				$USER_KEY				= $encrypto->get_hash();
				
				$CHANNEL				= sanitize_text_field($csv_data['CHANNEL']);
				$FORECASTREGDESC		= sanitize_text_field($csv_data['FORECASTREGDESC']);
				$SALES_REP				= sanitize_text_field($csv_data['SALES REP']);
				$CUSTOMER_NUMBER		= sanitize_text_field($csv_data['CUSTOMER #']);
				$CUSTOMER_NAME			= sanitize_text_field($csv_data['CUSTOMER NAME']);
				$FIRST					= sanitize_text_field($csv_data['FIRST']);
				$LAST					= sanitize_text_field($csv_data['LAST']);
				$EMAIL					= sanitize_email($csv_data['EMAIL']);
				$PEBBLE_BEACH_PROGRAM	= intval($csv_data['PEBBLE BEACH PROGRAM']) == 1 ? 1 : 0;
				$PEBBLE_BEACH_SALES		= floatval($csv_data['PEBBLE BEACH SALES']);
				$STAFF_MEMBER			= intval($csv_data['STAFF MEMBER']) == 1 ? 1 : 0;


				// $cook_string	= encryptomatic($implode,"encrypt");
				$data			= array(
					'USER KEY'				=> $USER_KEY,
					'CHANNEL'				=> $CHANNEL,
					'FORECASTREGDESC'		=> $FORECASTREGDESC,
					'SALES REP'				=> $SALES_REP,
					'CUSTOMER #'			=> $CUSTOMER_NUMBER,
					'CUSTOMER NAME'			=> $CUSTOMER_NAME,
					'FIRST'					=> $FIRST,
					'LAST'					=> $LAST,
					'EMAIL'					=> $EMAIL,
					'PEBBLE BEACH PROGRAM'	=> $PEBBLE_BEACH_PROGRAM,
					'PEBBLE BEACH SALES'	=> $PEBBLE_BEACH_SALES,
					'STAFF MEMBER'			=> $STAFF_MEMBER,
				);

				$wpdb->insert( SRIXON_USER_TABLE, $data );
				
				// insert_custom_table_values('test_table', $data);
				
				if( isset($email_users) && !empty($email_users) ){
					$decrypto		= new Encryptomatic($USER_KEY, 'decrypt');
					$USER_KEY		= $decrypto->get_hash();
					$email			= new Welcome_Email_Template($FIRST, $LAST, $EMAIL, $USER_KEY);
					$email->compose_email();
					$email->send_email();
				}
				
				$imported++;
			} else {
				$skipped++;
			}
		}

        if (file_exists($file)) {
            @unlink($file);
        }

        $exec_time = microtime(true) - $time_start;

        if ($skipped) {
            $this->log['notice'][] = "<b>Skipped {$skipped} users (most likely due to duplicate email addresses).</b>";
        }
        $this->log['notice'][] = sprintf("<b>Imported {$imported} users in %.2f seconds.</b>", $exec_time);
        $this->print_messages();
    }
	
	protected function generate_user_key($length = 7) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
    /**
     * Try to split lines of text correctly regardless of the platform the text
     * is coming from.
     */
    function split_lines($text) {
        $lines = preg_split("/(\r\n|\n|\r)/", $text);
        return $lines;
    }

    /**
     * Convert date in CSV file to 1999-12-31 23:52:00 format
     *
     * @param string $data
     * @return string
     */
    function parse_date($data) {
        $timestamp = strtotime($data);
        if (false === $timestamp) {
            return '';
        } else {
            return date('Y-m-d H:i:s', $timestamp);
        }
    }

    /**
     * Delete BOM from UTF-8 file.
     *
     * @param string $fname
     * @return void
     */
    function stripBOM($fname) {
        $res = fopen($fname, 'rb');
        if (false !== $res) {
            $bytes = fread($res, 3);
            if ($bytes == pack('CCC', 0xef, 0xbb, 0xbf)) {
                $this->log['notice'][] = 'Getting rid of byte order mark...';
                fclose($res);

                $contents = file_get_contents($fname);
                if (false === $contents) {
                    trigger_error('Failed to get file contents.', E_USER_WARNING);
                }
                $contents = substr($contents, 3);
                $success = file_put_contents($fname, $contents);
                if (false === $success) {
                    trigger_error('Failed to put file contents.', E_USER_WARNING);
                }
            } else {
                fclose($res);
            }
        } else {
            $this->log['error'][] = 'Failed to open file, aborting.';
        }
    }
}

?>

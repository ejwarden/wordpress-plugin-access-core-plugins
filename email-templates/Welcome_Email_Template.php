<?php

class Welcome_Email_Template {
	
	protected	$first_name 	= '',
				$last_name 		= '',
				$user_key 		= '',
				$email			= '',
				$html			= '';
	
	public function __construct( $fname, $lname, $eeemail, $ukey )
	{
		$this->first_name		= $fname;
		$this->last_name		= $lname;
		$this->user_key			= $ukey;
		$this->email			= $eeemail;
	}
	
	public function compose_email( $recover = false )
	{
		ob_start();
?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		    <head>
		        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		        <title>Srixon - Journey to Better - Welcome!</title>
		        <style type="text/css">
		            /***********
		            Originally based on The MailChimp Reset from Fabio Carneiro, MailChimp User Experience Design
		            More info and templates on Github: https://github.com/mailchimp/Email-Blueprints
		            http://www.mailchimp.com &amp; http://www.fabio-carneiro.com
		            INLINE: Yes.
		            ***********/
		            /* Client-specific Styles */
		            #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
		            body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
		            /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
		            .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
		            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
		            #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		            /* End reset */
		            /* Some sensible defaults for images
		            1. "-ms-interpolation-mode: bicubic" works to help ie properly resize images in IE. (if you are resizing them using the width and height attributes)
		            2. "border:none" removes border when linking images.
		            3. Updated the common Gmail/Hotmail image display fix: Gmail and Hotmail unwantedly adds in an extra space below images when using non IE browsers. You may not always want all of your images to be block elements. Apply the "image_fix" class to any image you need to fix.
		            Bring inline: Yes.
		            */
		            img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
		            a img {border:none;}
		            .image_fix {display:block;}
		            /** Yahoo paragraph fix: removes the proper spacing or the paragraph (p) tag. To correct we set the top/bottom margin to 1em in the head of the document. Simple fix with little effect on other styling. NOTE: It is also common to use two breaks instead of the paragraph tag but I think this way is cleaner and more semantic. NOTE: This example recommends 1em. More info on setting web defaults: http://www.w3.org/TR/CSS21/sample.html or http://meiert.com/en/blog/20070922/user-agent-style-sheets/
		            Bring inline: Yes.
		            **/
		            p {margin: 1em 0;}
		            /** Hotmail header color reset: Hotmail replaces your header color styles with a green color on H2, H3, H4, H5, and H6 tags. In this example, the color is reset to black for a non-linked header, blue for a linked header, red for an active header (limited support), and purple for a visited header (limited support).  Replace with your choice of color. The !important is really what is overriding Hotmail's styling. Hotmail also sets the H1 and H2 tags to the same size.
		            Bring inline: Yes.
		            **/
		            h1, h2, h3, h4, h5, h6 {color: black !important;}
		            h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}
		            h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
		            color: red !important; /* Preferably not the same color as the normal header link color.  There is limited support for psuedo classes in email clients, this was added just for good measure. */
		            }
		            h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
		            color: purple !important; /* Preferably not the same color as the normal header link color. There is limited support for psuedo classes in email clients, this was added just for good measure. */
		            }
		            /***************************************************
		            ****************************************************
		            MOBILE TARGETING
		            Use @media queries with care.  You should not bring these styles inline -- so it's recommended to apply them AFTER you bring the other stlying inline.
		            Note: test carefully with Yahoo.
		            Note 2: Don't bring anything below this line inline.
		            ****************************************************
		            ***************************************************/
		            /* NOTE: To properly use @media queries and play nice with yahoo mail, use attribute selectors in place of class, id declarations.
		            table[class=classname]
		            Read more: http://www.campaignmonitor.com/blog/post/3457/media-query-issues-in-yahoo-mail-mobile-email/
		            */
		            @media only screen and (max-device-width: 480px) {
		            /* A nice and clean way to target phone numbers you want clickable and avoid a mobile phone from linking other numbers that look like, but are not phone numbers.  Use these two blocks of code to "unstyle" any numbers that may be linked.  The second block gives you a class to apply with a span tag to the numbers you would like linked and styled.
		            Inspired by Campaign Monitor's article on using phone numbers in email: http://www.campaignmonitor.com/blog/post/3571/using-phone-numbers-in-html-email/.
		            Step 1 (Step 2: line 224)
		            */
		            a[href^="tel"], a[href^="sms"] {
		            text-decoration: none;
		            color: black; /* or whatever your want */
		            pointer-events: none;
		            cursor: default;
		            }
		            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
		            text-decoration: default;
		            color: orange !important; /* or whatever your want */
		            pointer-events: auto;
		            cursor: default;
		            }
		            }
		            /* More Specific Targeting */
		            @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
		            /* You guessed it, ipad (tablets, smaller screens, etc) */
		            /* Step 1a: Repeating for the iPad */
		            a[href^="tel"], a[href^="sms"] {
		            text-decoration: none;
		            color: blue; /* or whatever your want */
		            pointer-events: none;
		            cursor: default;
		            }
		            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
		            text-decoration: default;
		            color: orange !important;
		            pointer-events: auto;
		            cursor: default;
		            }
		            }
		            @media only screen and (-webkit-min-device-pixel-ratio: 2) {
		            /* Put your iPhone 4g styles in here */
		            }
		            /* Following Android targeting from:
		            http://developer.android.com/guide/webapps/targeting.html
		            http://pugetworks.com/2011/04/css-media-queries-for-targeting-different-mobile-devices/  */
		            @media only screen and (-webkit-device-pixel-ratio:.75){
		            /* Put CSS for low density (ldpi) Android layouts in here */
		            }
		            @media only screen and (-webkit-device-pixel-ratio:1){
		            /* Put CSS for medium density (mdpi) Android layouts in here */
		            }
		            @media only screen and (-webkit-device-pixel-ratio:1.5){
		            /* Put CSS for high density (hdpi) Android layouts in here */
		            }
		            /* end Android targeting */
		        </style>
		    </head>
		    <body style="width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;">
		        <!-- Wrapper/Container Table: Use a wrapper table to control the width and the background color consistently of your email. Use this approach instead of setting attributes on the body tag. -->
		        <table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; background:#66717E;">
		            <tbody>
		                <tr>
		                    <td>
		                    <!-- Tables are the most common way to format your email consistently. Set your table widths inside cells and in most cases reset cellpadding, cellspacing, and border to zero. Use nested tables as a way to space effectively in your message. -->
		                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;width:600px;">
		                        <tbody>
		                            <tr>
		                                <td width="600" valign="top" align="center" colspan="2">
		                                    <a href="http://www.srixon.com/" title="Srixon Home Page" style="outline:none; text-decoration:none;">
		                                        <img src="<?php echo get_stylesheet_directory_uri().'/' ?>assets/email-template/top-banner.gif" class="image_fix" style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;border:none;display:block;font-family: 'Gill Sans', 'Gill Sans MT', Calibri, sans-serif;text-align: center;" alt="Srixon, Cleveland Golf, XXIO">
		                                    </a>
		                                </td>
		                            </tr><!-- logos -->
		                            <tr>
		                                <td width="600" valign="top" align="center" colspan="2">
		                                    <img src="<?php echo get_stylesheet_directory_uri().'/' ?>assets/email-template/top-photo.gif" class="image_fix" style="outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;border:none;display:block;font-family: 'Gill Sans', 'Gill Sans MT', Calibri, sans-serif;text-align: center;" alt="2015 is Going to Be a Great Year For Golf">
		                                </td>
		                            </tr><!-- top banner -->
		                            <tr>
		                                <td width="600" valign="top" align="center" style="background:#ffffff;padding:30px;" colspan="2">
											<?php if ( $recover ): ?>
			                                    <p style="margin-top:10px; margin-bottom: 5px; font-weight: bold; font-family: 'Open Sans', 'Open Sans', Arial, sans-serif; color:#323437; font-size:18px; line-height:28px; mso-line-height-rule: exactly;">
			                                        You've requested that your personalized site key for the Srixon Partner Programs be sent to this email address.
			                                    </p>
											<?php else: ?>
		                                    <p style="margin-top:10px; margin-bottom: 5px; font-weight: bold; font-family: 'Open Sans', 'Open Sans', Arial, sans-serif; color:#323437; font-size:18px; line-height:28px; mso-line-height-rule: exactly;">
		                                        Welcome to the Srixon Partner Program, <?php echo $this->first_name; ?>!
		                                    </p>
		                                    <p style="margin-bottom:30px; font-size:14px ; color:#636363; font-family: 'Open Sans', Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">
												As a Green Grass partner, you can now access our client portal at <a href="http://www.srixon.com/partnerprograms" style="font-size:14px ; color:#ec2c28; font-family: 'Open Sans', Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">http://www.srixon.com/partnerprograms</a>,
												where you can track your Pebble beach progress, print marketing materials, and more!
											</p>
											<?php endif; ?>
		                                    <p style="margin-bottom:30px; font-size:14px ; color:#636363; font-family: 'Open Sans', Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">
												Your personalized site key is <strong><?php echo $this->user_key; ?></strong>. Please make a note of this key, as it will serve as your identifying information on the web portal.
											</p>
		                                </td>
		                            </tr><!-- overview paragraph -->
		                            <tr>
		                                <td width="600" valign="top" align="center" style="background:#ffffff; padding:30px;" colspan="2">
		                                    <p style="margin-top:20px; margin-bottom: 0px; font-weight: normal; font-family: 'Open Sans', 'Open Sans', Arial, sans-serif;  color:#636363; font-size:13px; line-height:28px; mso-line-height-rule: exactly;">
		                                        &copy; 2015 <a href="http://www.srixon.com/" style="color:#ec2c28;">SRI DUNLOP SPORTS CO.LTD.</a> All Rights Reserved.
		                                    </p>
		                                </td>
		                            </tr><!-- bottom copy -->
		                        </tbody>
		                    </table>
		                    </td>
		                </tr>
		            </tbody>
		        </table>
		        <!-- End of wrapper table -->
		    </body>
		</html>
<?php
		$this->html	= ob_get_clean();
	}
	
	public function send_email( $recover = false )
	{
		$to			= $this->email;
		$subject	= $recover ? 'Recover Your Key from the Srixon Partner Programs' : 'Welcome to the Srixon Partner Programs Site, '.$this->first_name.'!';
		$message	= $this->html;
		$headers	= array('From: Srixon Partner Programs <noreply@srixon.com>');
		
		add_filter( 'wp_mail_content_type', 'set_html_content_type' );
		wp_mail( $to, $subject, $message, $headers );
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
	}
}
?>
























